-- View: mobingo.auth_net_transactions

-- DROP VIEW mobingo.auth_net_transactions;

CREATE OR REPLACE VIEW mobingo.auth_net_transactions
 AS
 SELECT a.action_id,
    c.status AS action_status,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    a.transaction_id,
    a.invoice_number,
    b.email,
    d.cash_amount,
    d.payment_provider,
    d.status AS donation_status
   FROM mobingo.authorize_net a,
    mobingo.users b,
    mobingo.user_actions c,
    mobingo.payments d
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.action_id = d.action_id
  ORDER BY a.created_at DESC;

ALTER TABLE mobingo.auth_net_transactions
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo.auth_net_transactions TO rubyplay;
GRANT ALL ON TABLE mobingo.auth_net_transactions TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo.auth_net_transactions TO rubyplay;

