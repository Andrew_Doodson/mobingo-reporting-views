-- View: mobingo.bet_transactions_view

-- DROP VIEW mobingo.bet_transactions_view;

CREATE OR REPLACE VIEW mobingo.bet_transactions_view
 AS
 SELECT a.action_id,
    a.status AS bet_status,
    a.bet_id,
    a.bet_amount,
    a.game_name,
    a.paytable_used,
    a.win_amount,
    a.skill_check,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    a.user_id,
    b.email
   FROM mobingo.bets a,
    mobingo.users b
  WHERE a.user_id::text = b.user_id::text
  ORDER BY a.created_at DESC;

ALTER TABLE mobingo.bet_transactions_view
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo.bet_transactions_view TO rubyplay;
GRANT ALL ON TABLE mobingo.bet_transactions_view TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo.bet_transactions_view TO rubyplay;

