-- View: mobingo.Bonus Points View

-- DROP VIEW mobingo."Bonus Points View";

CREATE OR REPLACE VIEW mobingo."Bonus Points View"
 AS
 SELECT a.action_id,
    a.additional_notes,
    a.accounts_adjustment_type,
    a.transaction_id,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    b.amount AS points_amount,
    c.email,
    c.user_id,
    'dashboard_bonus'::character varying AS promo_code,
    'dashboard_bonus'::character varying AS voucherify_type,
    'dashboard_bonus'::character varying AS promo_type,
    d.status
   FROM mobingo.accounts_adjustments a,
    mobingo.points_transactions b,
    mobingo.users c,
    mobingo.user_actions d
  WHERE a.action_id = b.action_id AND b.user_id::text = c.user_id::text AND a.accounts_adjustment_type::text <> 'Null'::text AND a.action_id = d.action_id
UNION ALL
 SELECT a.action_id,
    NULL::character varying AS additional_notes,
    NULL::character varying AS accounts_adjustment_type,
    NULL::uuid AS transaction_id,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    a.points_amount,
    b.email,
    b.user_id,
    a.promo_code,
    a.voucherify_type,
    a.promo_type,
    c.status
   FROM mobingo.promos a,
    mobingo.users b,
    mobingo.user_actions c
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.promo_code::text <> 'DEFAULT'::text;

ALTER TABLE mobingo."Bonus Points View"
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo."Bonus Points View" TO rubyplay;
GRANT ALL ON TABLE mobingo."Bonus Points View" TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo."Bonus Points View" TO rubyplay;

