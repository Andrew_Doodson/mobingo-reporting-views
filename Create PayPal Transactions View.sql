-- View: mobingo.paypal_transactions

-- DROP VIEW mobingo.paypal_transactions;

CREATE OR REPLACE VIEW mobingo.paypal_transactions
 AS
 SELECT a.action_id,
    c.status AS action_status,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    a.paypal_order_id,
    a.paypal_capture_id,
    b.email,
    d.cash_amount,
    d.payment_provider,
    d.status AS donation_status
   FROM mobingo.paypal a,
    mobingo.users b,
    mobingo.user_actions c,
    mobingo.payments d
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.action_id = d.action_id
  ORDER BY a.created_at DESC;

ALTER TABLE mobingo.paypal_transactions
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo.paypal_transactions TO rubyplay;
GRANT ALL ON TABLE mobingo.paypal_transactions TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo.paypal_transactions TO rubyplay;

