-- View: mobingo.Cashout/Payment Union View

-- DROP VIEW mobingo."Cashout/Payment Union View";

CREATE OR REPLACE VIEW mobingo."Cashout/Payment Union View"
 AS
 SELECT a.user_id,
    a.action_id,
    b.email,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    a.points_amount,
    a.status,
    c.action_type,
    a.payment_provider,
    a.promo_id
   FROM mobingo.payments a,
    mobingo.users b,
    mobingo.user_actions c
  WHERE a.user_id::text = b.user_id::text AND a.action_id = c.action_id
UNION
 SELECT a.user_id,
    a.action_id,
    b.email,
    timezone('US/Central'::text, a.created_at) AS created_at_local_time,
    timezone('US/Central'::text, a.last_modified_at) AS last_modified_at_local_time,
    a.points_amount,
    a.status,
    c.action_type,
    NULL::character varying AS payment_provider,
    NULL::uuid AS promo_id
   FROM mobingo.cash_outs a,
    mobingo.users b,
    mobingo.user_actions c
  WHERE a.user_id::text = b.user_id::text AND a.action_id = c.action_id
  ORDER BY 4 DESC;

ALTER TABLE mobingo."Cashout/Payment Union View"
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo."Cashout/Payment Union View" TO rubyplay;
GRANT ALL ON TABLE mobingo."Cashout/Payment Union View" TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo."Cashout/Payment Union View" TO rubyplay;

