-- View: mobingo.user_attribution_vw

-- DROP VIEW mobingo.user_attribution_vw;

CREATE OR REPLACE VIEW mobingo.user_attribution_vw
 AS
 SELECT a.user_id,
    a.promo_code,
    btrim(regexp_match(a.joining_url::text, '.*[&?]utm_campaign=([^&]+).*'::text, ''::text)::character varying::text, '{}'::text) AS utm_campaign,
    btrim(regexp_match(a.joining_url::text, '.*[&?]utm_medium=([^&]+).*'::text, ''::text)::character varying::text, '{}'::text) AS utm_medium,
    btrim(regexp_match(a.joining_url::text, '.*[&?]utm_source=([^&]+).*'::text, ''::text)::character varying::text, '{}'::text) AS utm_source,
    a.referral_url,
    a.joining_url,
    a.referrer,
    a.owner_organization
   FROM mobingo.user_attribution a;

ALTER TABLE mobingo.user_attribution_vw
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo.user_attribution_vw TO rubyplay;
GRANT ALL ON TABLE mobingo.user_attribution_vw TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo.user_attribution_vw TO rubyplay;

