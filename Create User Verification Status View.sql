-- View: mobingo.user_verification_status_view

-- DROP VIEW mobingo.user_verification_status_view;

CREATE OR REPLACE VIEW mobingo.user_verification_status_view
 AS
 SELECT user_phone_verifications.verification_id,
    user_phone_verifications.user_id,
    user_phone_verifications.created_at,
    user_phone_verifications.ip_address,
    user_phone_verifications.email,
    user_phone_verifications.phone_number,
    user_phone_verifications.status,
    user_phone_verifications.action_id,
    user_phone_verifications.raw_response::json -> 'phone.line_type'::text AS phone_line_type,
    user_phone_verifications.raw_response::json -> 'email.valid'::text AS email_valid,
    user_phone_verifications.raw_response::json -> 'email.to_name'::text AS email_to_name,
    user_phone_verifications.raw_response::json -> 'email.first_seen_days'::text AS email_first_seen_days,
    user_phone_verifications.raw_response::json -> 'phone.valid'::text AS phone_valid,
    user_phone_verifications.raw_response::json -> 'phone.carrier'::text AS phone_carrier,
    user_phone_verifications.raw_response::json -> 'phone.country_code'::text AS phone_country_code,
    user_phone_verifications.raw_response::json -> 'phone.last_seen_days'::text AS phone_last_seen_days,
    user_phone_verifications.raw_response::json -> 'phone.email.first_seen_days'::text AS phone_email_first_seen_days,
    user_phone_verifications.raw_response::json -> 'phone.to_name'::text AS phone_to_name,
    user_phone_verifications.raw_response::json -> 'identity_network_score'::text AS identity_network_score,
    user_phone_verifications.raw_response::json -> 'identity_risk_score'::text AS identity_risk_score
   FROM mobingo.user_phone_verifications;

ALTER TABLE mobingo.user_verification_status_view
    OWNER TO rubyplay;

GRANT SELECT ON TABLE mobingo.user_verification_status_view TO rubyplay;
GRANT ALL ON TABLE mobingo.user_verification_status_view TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE mobingo.user_verification_status_view TO rubyplay;

